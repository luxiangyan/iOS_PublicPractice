# LXYComponentExtension

[![CI Status](https://img.shields.io/travis/lxy/LXYComponentExtension.svg?style=flat)](https://travis-ci.org/lxy/LXYComponentExtension)
[![Version](https://img.shields.io/cocoapods/v/LXYComponentExtension.svg?style=flat)](https://cocoapods.org/pods/LXYComponentExtension)
[![License](https://img.shields.io/cocoapods/l/LXYComponentExtension.svg?style=flat)](https://cocoapods.org/pods/LXYComponentExtension)
[![Platform](https://img.shields.io/cocoapods/p/LXYComponentExtension.svg?style=flat)](https://cocoapods.org/pods/LXYComponentExtension)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LXYComponentExtension is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LXYComponentExtension'
```

## Author

lxy, 1556937213@qq.com

## License

LXYComponentExtension is available under the MIT license. See the LICENSE file for more info.
