//
//  Extension+NotificationCenter.swift
//  CityPlatform
//
//  Created by GY on 2018/12/21.
//  Copyright © 2018年 MC. All rights reserved.
//

import Foundation
import UIKit


/// 可继续扩展
public enum MCNotification: String {
    
    /// 微信登录
    case wxLogin
    /// 支付结果
    case payResult
    /// 刷新所有数据
    case reloadAllData
    /// 新增/修改收货地址
    case modifyAddress
    /// 订单状态发生改变
    case orderStatusChange
    
    
    private var stringValue: String {
        return "MC" + rawValue
    }
    
    public var notificationName: NSNotification.Name {
        return NSNotification.Name(stringValue)
    }
}





extension NotificationCenter {
    
    public static let shared = NotificationCenter.default
    

    /// 发送通知
    public func post(_ name: MCNotification, object:Any? = nil) {
       
        NotificationCenter.default.post(name: name.notificationName, object: object, userInfo: nil)
    }
    
    /// 监听通知
    public func addObserver(_ name: MCNotification,vc: Any,selector : Selector, object: Any? = nil) {
        NotificationCenter.default.addObserver(vc, selector: selector, name: name.notificationName, object: object)
    }
    
    /// 移除通知
    public func remove(_ name: MCNotification,vc: Any, object: Any? = nil) {
        NotificationCenter.default.removeObserver(vc, name: name.notificationName, object: object)
    }
}
