//
//  UIFont+Extension.swift
//  MCAPI
//
//  Created by MC on 2018/11/26.
//

import Foundation


extension UIFont {
    
    public static let MC10 = UIFont.systemFont(ofSize: 10)
    public static let MC11 = UIFont.systemFont(ofSize: 11)
    public static let MC12 = UIFont.systemFont(ofSize: 12)
    public static let MC13 = UIFont.systemFont(ofSize: 13)
    public static let MC14 = UIFont.systemFont(ofSize: 14)
    public static let MC15 = UIFont.systemFont(ofSize: 15)
    public static let MC16 = UIFont.systemFont(ofSize: 16)
    public static let MC17 = UIFont.systemFont(ofSize: 17)
    public static let MC18 = UIFont.systemFont(ofSize: 18)
    public static let MC19 = UIFont.systemFont(ofSize: 19)
    public static let MC20 = UIFont.systemFont(ofSize: 20)
    public static let MC21 = UIFont.systemFont(ofSize: 21)
    public static let MC22 = UIFont.systemFont(ofSize: 22)
    public static let MC23 = UIFont.systemFont(ofSize: 23)
    public static let MC24 = UIFont.systemFont(ofSize: 24)
    public static let MC25 = UIFont.systemFont(ofSize: 25)
    public static let MC26 = UIFont.systemFont(ofSize: 26)
    public static let MC27 = UIFont.systemFont(ofSize: 27)
    public static let MC28 = UIFont.systemFont(ofSize: 28)
    public static let MC29 = UIFont.systemFont(ofSize: 29)
    public static let MC30 = UIFont.systemFont(ofSize: 30)
    public static let MC31 = UIFont.systemFont(ofSize: 31)
    public static let MC32 = UIFont.systemFont(ofSize: 32)

    
    
    // 加粗
    public static let MCBold12 = UIFont.boldSystemFont(ofSize: 12)
    public static let MCBold13 = UIFont.boldSystemFont(ofSize: 13)
    public static let MCBold14 = UIFont.boldSystemFont(ofSize: 14)
    public static let MCBold15 = UIFont.boldSystemFont(ofSize: 15)
    public static let MCBold16 = UIFont.boldSystemFont(ofSize: 16)
    public static let MCBold17 = UIFont.boldSystemFont(ofSize: 17)
    public static let MCBold18 = UIFont.boldSystemFont(ofSize: 18)
    public static let MCBold19 = UIFont.boldSystemFont(ofSize: 19)
    public static let MCBold20 = UIFont.boldSystemFont(ofSize: 20)
    public static let MCBold21 = UIFont.boldSystemFont(ofSize: 21)
    public static let MCBold22 = UIFont.boldSystemFont(ofSize: 22)

    
    
    
    
    //字体
    public static func MC(_ font : CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: font)
    }
    
    public static func MCBold(_ font : CGFloat) -> UIFont {
        return UIFont.boldSystemFont(ofSize: font)
    }

}
