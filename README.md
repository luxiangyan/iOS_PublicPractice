# LXYPublicPractice

[![CI Status](https://img.shields.io/travis/lxy/LXYPublicPractice.svg?style=flat)](https://travis-ci.org/lxy/LXYPublicPractice)
[![Version](https://img.shields.io/cocoapods/v/LXYPublicPractice.svg?style=flat)](https://cocoapods.org/pods/LXYPublicPractice)
[![License](https://img.shields.io/cocoapods/l/LXYPublicPractice.svg?style=flat)](https://cocoapods.org/pods/LXYPublicPractice)
[![Platform](https://img.shields.io/cocoapods/p/LXYPublicPractice.svg?style=flat)](https://cocoapods.org/pods/LXYPublicPractice)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LXYPublicPractice is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LXYPublicPractice'
```

## Author

lxy, 1556937212@qq.com

## License

LXYPublicPractice is available under the MIT license. See the LICENSE file for more info.
