//
//  LXYAppstore.swift
//  FBSnapshotTestCase
//
//  Created by GY on 2019/2/25.
//

import Foundation

public class LXYAppstore : NSObject {
    
    // 单例
    public static let shared = LXYAppstore.init()
    
    /**
     * 去App Store评分
     * @param AppID: app的Id
     */
    public func toScore(_ AppID: String) {
        let urlStr = "itms-apps://itunes.apple.com/app/id\(AppID)?action=write-review"
        UIApplication.shared.openURL(URL.init(string: urlStr)!)
    }
    
    /**
     * 去App Store下载
     */
    
    public func toUpdate(_ AppID: String) {
        
        let urlStr = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=\(AppID)"
        UIApplication.shared.openURL(URL.init(string: urlStr)!)
        
    }
    
}
