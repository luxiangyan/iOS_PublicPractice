#
# Be sure to run `pod lib lint LXYPublicPractice.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LXYPublicPractice'
  s.version          = '0.0.9'
  s.summary          = '练习的demo'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/luxiangyan'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lxy' => '1556937212@qq.com' }
  s.source           = { :git => 'https://gitee.com/luxiangyan/iOS_PublicPractice.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.swift_version = '4.2'
  
  
  # 添加目录文件
  s.subspec 'LXYAppstore' do |ss|
     ss.source_files = "LXYPublicPractice/Classes/LXYAppstore.swift"
  end

  s.subspec 'LXYCountDown' do |ss|
      ss.source_files = "LXYPublicPractice/Classes/LXYCountDown.swift"
  end
  
  # 添加资源文件
  s.subspec 'LXYImage' do |ss|
      
      ss.ios.resource_bundle = { 'LXYImageBundle' => 'LXYPublicPractice/Assets/**/*.png' }
      
  end

#s.source_files = 'LXYPublicPractice/Classes/**/*'


  # 加载依赖库
  s.dependency 'LXYComponentExtension'
  
  # s.resource_bundles = {
  #   'LXYPublicPractice' => ['LXYPublicPractice/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
